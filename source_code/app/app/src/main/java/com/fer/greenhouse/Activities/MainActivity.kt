package com.fer.greenhouse.Activities

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.fer.greenhouse.Services.BackgroundCheckService
import com.fer.greenhouse.Models.*
import com.fer.greenhouse.R
import com.fer.greenhouse.REST.ApiClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import java.sql.Timestamp


class MainActivity : AppCompatActivity() {
    private var client = ApiClient.getClient
    private var db = DatabaseHelper(this)
    private var data = mutableListOf<Reading>()
    private var alerts = mutableListOf<Alerts>()
    private var lastReading = mutableListOf<Reading>()
    private var green = 0
    private var error = 0L
    private var hasConnection = false
    private var noOfReadings: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        stopService(Intent(this, BackgroundCheckService::class.java))

        val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
        val username = storage.getString("username", "")
        noOfReadings = storage.getInt("noOfReadings", 24)
        hasConnection = storage.getBoolean("hasConnection", true)
        Log.d("hasConnection", hasConnection.toString())
        error = storage.getLong("error", 1800000)

        Log.d("username", username!!)
        tv_welcome.append(username)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        update.setOnClickListener {
            db.updateAlerts(
                "Radoboj",
                0.toFloat(),
                10000.toFloat(),
                0.toFloat(),
                10000.toFloat(),
                0.toFloat(),
                10000.toFloat(),
                0.toFloat(),
                10000.toFloat()
            )
            tv_text.text = "Očitanja: \n"
            tv_alerts.text = "Ograničenja: \n"
            readData(username)
            checkAlerts()
        }

        Log.d("noOfReadings", noOfReadings.toString())
        readData(username)

        for (reading in lastReading) {
            if (reading.sensor == "temp") {
                greenhouseCard(reading.locationId, reading.location, reading.reading.toString())
            }
        }
        initListeners(username)
        initGreenhouseCardListener()
//        if (hasConnection) {
        checkAlerts()
//        }

//        Handler().postDelayed({
        if (!hasConnection) {
            tv_network_error.text = "Nema internetske veze!"
            tv_network_error.setTextColor(Color.parseColor("#FF6969"))
        }
//        }, 100)
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Izlazak iz aplikacije")
        builder.setMessage("Jeste li sigurni da želite izaći iz aplikacije?")

        builder.setPositiveButton("Da") { _, _ ->
            startService(Intent(this, BackgroundCheckService::class.java))
            finish()
        }

        builder.setNegativeButton("Ne") { _, _ ->

        }

        builder.show()
    }

    override fun onResume() {
        super.onResume()
        if (hasConnection) {
            resetAlerts()
        }
        stopService(Intent(this, BackgroundCheckService::class.java))
    }

    private fun resetAlerts() {
        if (hasConnection) {
            checkAlerts()
        }
    }

    //        if (username != null) {
//            getGreenhouses(username)
//            Handler().postDelayed({
//                for (greenhouse in greenhouses){
//                    println(greenhouse.greenhouseId)
//                    Handler().postDelayed({
//                        getReadings(username, greenhouse.greenhouseId!!, "temp")
//                        for (reading in readings){
//                            println(reading.reading)
//                        }
//                    }, 2000)
//                }
//            }, 2000)
//        }
//    }
//
//    fun getGreenhouses(username: String) {
//        val call = client.getGreenhouses(username)
//        call.enqueue(object : Callback<List<GreenhouseId>> {
//            override fun onFailure(call: Call<List<GreenhouseId>>, t: Throwable) {
//                Toast.makeText(this@MainActivity, "Greška!\n" + t.message, Toast.LENGTH_SHORT)
//                    .show()
//            }
//
//            override fun onResponse(call: Call<List<GreenhouseId>>?, response: Response<List<GreenhouseId>>?) {
//                if (response == null) {
//                    Toast.makeText(this@MainActivity, "Greška!", Toast.LENGTH_SHORT)
//                        .show()
//                } else {
//                    if (response.isSuccessful) {
//                        greenhouses = response.body()!!
//                    }
//                }
//            }
//        })
//    }
//
//    fun getReadings(username: String, greenhouseId: Int, type: String) {
//        val call = client.getReadings(username, greenhouseId, type)
//        call.enqueue(object : Callback<List<ReadingDate>> {
//            override fun onFailure(call: Call<List<ReadingDate>>, t: Throwable) {
//                Toast.makeText(this@MainActivity, "Greška!\n" + t.message, Toast.LENGTH_SHORT)
//                    .show()
//            }
//
//            override fun onResponse(call: Call<List<ReadingDate>>?, response: Response<List<ReadingDate>>?) {
//                if (response == null) {
//                    Toast.makeText(this@MainActivity, "Greška!", Toast.LENGTH_SHORT)
//                        .show()
//                } else {
//                    if (response.isSuccessful) {
//                        readings = response.body()!!
//                    }
//                }
//            }
//        })
//    }
    private fun initListeners(username: String) {
        pullToRefresh.setOnRefreshListener {
            val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
            noOfReadings = storage.getInt("noOfReadings", 24)
            Log.d("noOfReadings", noOfReadings.toString())

            readData(username)

            checkAlerts()

            if (hasConnection) {
                tv_network_error.visibility = View.GONE
            }

            pullToRefresh.isRefreshing = false
        }

        btn_settings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    private fun readData(username: String) {
        Log.d("fun", "fun")
        if (hasConnection) {
            Log.d("if", "if")
            try {
                Log.d("try", "try")
                db.deleteTableDataReadings()
                val call: Call<List<GreenhouseId>> = client.getGreenhouses(username)
                val greenhouses: List<GreenhouseId> = call.execute().body()!!
                for (greenhouse in greenhouses) {
                    db.initAlerts(greenhouse.greenhouseId!!, greenhouse.location!!)
                    val callReadingsTemp: Call<List<ReadingDate>> =
                        client.getReadings(username, greenhouse.greenhouseId, "temp", noOfReadings)
                    val readingsTemp: List<ReadingDate> = callReadingsTemp.execute().body()!!
                    for (reading in readingsTemp) {
                        val readingDb =
                            Reading(
                                greenhouse.greenhouseId,
                                greenhouse.location,
                                "temp",
                                reading.reading!!,
                                reading.date!!
                            )
                        db.insertDataReadings(readingDb)
                    }
                    val callReadingsAir: Call<List<ReadingDate>> =
                        client.getReadings(username, greenhouse.greenhouseId, "air", noOfReadings)
                    val readingsAir: List<ReadingDate> = callReadingsAir.execute().body()!!
                    for (reading in readingsAir) {
                        val readingDb =
                            Reading(
                                greenhouse.greenhouseId,
                                greenhouse.location,
                                "air",
                                reading.reading!!,
                                reading.date!!
                            )
                        db.insertDataReadings(readingDb)
                    }
                    val callReadingsSoil: Call<List<ReadingDate>> =
                        client.getReadings(username, greenhouse.greenhouseId, "soil", noOfReadings)
                    val readingsSoil: List<ReadingDate> = callReadingsSoil.execute().body()!!
                    for (reading in readingsSoil) {
                        val readingDb =
                            Reading(
                                greenhouse.greenhouseId,
                                greenhouse.location,
                                "soil",
                                reading.reading!!,
                                reading.date!!
                            )
                        db.insertDataReadings(readingDb)
                    }
                    val callReadingsLight: Call<List<ReadingDate>> =
                        client.getReadings(username, greenhouse.greenhouseId, "light", noOfReadings)
                    val readingsLight: List<ReadingDate> = callReadingsLight.execute().body()!!
                    for (reading in readingsLight) {
                        val readingDb =
                            Reading(
                                greenhouse.greenhouseId,
                                greenhouse.location,
                                "light",
                                reading.reading!!,
                                reading.date!!
                            )
                        db.insertDataReadings(readingDb)
                    }
                }
            } catch (e: Exception) {
                print(e)
            }
        }

        data = db.readDataReadings()
//        for (reading in data) {
//            tv_text.append("greenhouse: " + reading.location + ", sensor: " + reading.sensor + ", reading: " + reading.reading + ", timestamp:" + reading.timestamp + "\n")
//        }

        alerts = db.readAlerts()
//        for (alert in alerts) {
//            tv_alerts.append("greenhouse: " + alert.location + ", tempmin: " + alert.tempmin + ", tempmax: " + alert.tempmax + "\n")
//        }

        lastReading = db.readDataLastReading()
//        for (reading in lastReading) {
//            tv_lastreading.append("greenhouse: " + reading.location + ", sensor: " + reading.sensor + ", reading: " + reading.reading + ", timestamp:" + reading.timestamp + "\n")
//        }
    }

    private fun greenhouseCard(
        greenhouseId: Int,
        location: String,
        temp: String
    ) {
        val layoutInflater =
            baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val addView = layoutInflater.inflate(R.layout.greenhouse_card, null)
        addView.id = greenhouseId
        val tvLocation = addView.findViewById(R.id.tv_greenhouse_location) as TextView
        tvLocation.text = location
        val tvTemp = addView.findViewById(R.id.tv_temp) as TextView
        tvTemp.text = temp
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(0, 0, 0, 15)
        addView.layoutParams = layoutParams

        container.addView(addView)
    }

    private fun checkAlerts() {
        val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
        error = storage.getLong("error", 1800000)

        baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        try {
            for (alert in alerts) {
                val container = container.findViewById<LinearLayout>(alert.locationId)
                val status = container.findViewById(R.id.iv_status) as ImageView
                val temp = container.findViewById(R.id.tv_temp) as TextView
                for (reading in lastReading) {
                    if (reading.locationId == alert.locationId) {
                        if (reading.sensor == "temp") {
                            if (reading.reading < alert.tempmin || reading.reading > alert.tempmax) {
                                status.setColorFilter(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.colorAccentRed
                                    ), android.graphics.PorterDuff.Mode.SRC_IN
                                )
                            } else {
                                green++
                            }
                            temp.text = reading.reading.toString()
                            //                        if (reading.reading > alert.tempmin && reading.reading < alert.tempmax) {
                            //                            status.setColorFilter(
                            //                                ContextCompat.getColor(
                            //                                    applicationContext,
                            //                                    R.color.colorAccentGreen
                            //                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            //                            )
                            //                        }
                        }
                        if (reading.sensor == "air") {
                            if (reading.reading < alert.airmin || reading.reading > alert.airmax) {
                                status.setColorFilter(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.colorAccentRed
                                    ), android.graphics.PorterDuff.Mode.SRC_IN
                                )
                            } else {
                                green++
                            }
                            //                        if (reading.reading > alert.airmin && reading.reading < alert.airmax) {
                            //                            status.setColorFilter(
                            //                                ContextCompat.getColor(
                            //                                    applicationContext,
                            //                                    R.color.colorAccentGreen
                            //                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            //                            )
                            //                        }
                        }
                        if (reading.sensor == "soil") {
                            if (reading.reading < alert.soilmin || reading.reading > alert.soilmax) {
                                status.setColorFilter(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.colorAccentRed
                                    ), android.graphics.PorterDuff.Mode.SRC_IN
                                )
                            } else {
                                green++
                            }
                            //                        if (reading.reading > alert.soilmin && reading.reading < alert.soilmax) {
                            //                            status.setColorFilter(
                            //                                ContextCompat.getColor(
                            //                                    applicationContext,
                            //                                    R.color.colorAccentGreen
                            //                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            //                            )
                            //                        }
                        }
                        if (reading.sensor == "light") {
                            if (reading.reading < alert.lightmin || reading.reading > alert.lightmax) {
                                status.setColorFilter(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.colorAccentRed
                                    ), android.graphics.PorterDuff.Mode.SRC_IN
                                )
                            } else {
                                green++
                            }
                            //                        if (reading.reading > alert.lightmin && reading.reading < alert.lightmax) {
                            //                            status.setColorFilter(
                            //                                ContextCompat.getColor(
                            //                                    applicationContext,
                            //                                    R.color.colorAccentGreen
                            //                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            //                            )
                            //                        }
                        }
                        if (green == 4) {
                            status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentGreen
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        }
                        if (Timestamp(System.currentTimeMillis() - error) > reading.timestamp) {
                            tv_reading_error.text =
                                ("Nije bilo mjerenja u zadnjih " + (error / (1000 * 60)).toString() + " minuta!")
                            tv_reading_error.setTextColor(Color.parseColor("#FF6969"))
                            tv_reading_error.visibility = View.VISIBLE
                        }
                        if (Timestamp(System.currentTimeMillis() - error) <= reading.timestamp) {
                            tv_reading_error.visibility = View.GONE
                        }
                    }
                }
                green = 0
            }
        } catch (e: Exception) {
            print(e)
            tv_reading_error.text = "Nema staklenika za prikaz"
            tv_reading_error.visibility = View.VISIBLE
        }
    }

    private fun initGreenhouseCardListener() {
        baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        for (reading in lastReading) {
            val container = container.findViewById<LinearLayout>(reading.locationId)

            container.setOnClickListener {
                val intent = Intent(this, GreenhouseActivity::class.java)
                intent.putExtra("greenhouse_title", reading.location)
                startActivity(intent)
                overridePendingTransition(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out
                )
            }
        }
    }
}
