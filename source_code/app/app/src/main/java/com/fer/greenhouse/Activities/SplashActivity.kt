package com.fer.greenhouse.Activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import com.fer.greenhouse.Models.GreenhouseId
import com.fer.greenhouse.R
import com.fer.greenhouse.REST.ApiClient
import retrofit2.Call

class SplashActivity : Activity() {
    /** Duration of wait  */
    private val SPLASH_DISPLAY_LENGTH = 1000

    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
            val isLoggedIn = storage.getBoolean("isLoggedIn", false)
            val username = storage.getString("username", "")
            if (isLoggedIn && username != "") {
                try {
                    val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                    StrictMode.setThreadPolicy(policy)
                    val client = ApiClient.getClient
                    val checkCall: Call<List<GreenhouseId>> = client.getGreenhouses(username!!)
                    checkCall.execute()

                    val editor: SharedPreferences.Editor = storage.edit()
                    editor.putBoolean("hasConnection", true)
                    editor.apply()
                    Log.d("hasConnection", "true")
                } catch (e: Exception) {

                    val editor: SharedPreferences.Editor = storage.edit()
                    editor.putBoolean("hasConnection", false)
                    editor.apply()
                    Log.d("hasConnection", "false")
                } finally {
                    val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                    this@SplashActivity.startActivity(mainIntent)
                    overridePendingTransition(
                        android.R.anim.fade_in,
                        android.R.anim.fade_out
                    )
                }
            } else {
                val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                this@SplashActivity.startActivity(mainIntent)
                overridePendingTransition(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out
                )
            }
            finish()
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }
}