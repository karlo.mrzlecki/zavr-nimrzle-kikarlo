package com.fer.greenhouse.Models

class Alerts{
    var id: Int = 0
    var locationId: Int = 0
    var location: String = ""
    var airmin: Float = 0.toFloat()
    var airmax: Float = 0.toFloat()
    var lightmin: Float = 0.toFloat()
    var lightmax: Float = 0.toFloat()
    var soilmin: Float = 0.toFloat()
    var soilmax: Float = 0.toFloat()
    var tempmin: Float = 0.toFloat()
    var tempmax: Float = 0.toFloat()

    constructor(locationId: Int, location: String, airmin: Float, airmax: Float, lightmin: Float, lightmax: Float, soilmin: Float, soilmax: Float, tempmin: Float, tempmax: Float){
        this.locationId = locationId
        this.location = location
        this.airmin = airmin
        this.airmax = airmax
        this.lightmin = lightmin
        this.lightmin = lightmax
        this.soilmin = soilmin
        this.soilmax = soilmax
        this.tempmin = tempmin
        this.tempmax = tempmax

    }

    constructor(){
    }
}