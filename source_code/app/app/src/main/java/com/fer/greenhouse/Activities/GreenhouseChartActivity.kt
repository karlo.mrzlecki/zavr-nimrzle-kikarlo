package com.fer.greenhouse.Activities

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.fer.greenhouse.Models.DatabaseHelper
import com.fer.greenhouse.R
import kotlinx.android.synthetic.main.activity_greenhouse_chart.*
import org.eazegraph.lib.charts.ValueLineChart
import org.eazegraph.lib.models.ValueLinePoint
import org.eazegraph.lib.models.ValueLineSeries
import java.sql.Date
import java.util.*


class GreenhouseChartActivity : AppCompatActivity() {

    private var db = DatabaseHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greenhouse_chart)

        val greenhouse = intent.getStringExtra("greenhouse_title")
        val type = intent.getStringExtra("chart_type")
        val name = intent.getStringExtra("chart_name")
        Log.d("greenhouse", greenhouse!!)
        Log.d("type", type!!)
        Log.d("name", name!!)

        val readings = db.readReadingsByGreenhouseBySensor(greenhouse, type)

        tv_greenhouse_chart_title.text = ("$greenhouse, $name")

        val lineChart =
            findViewById(R.id.linechart) as ValueLineChart

        val series = ValueLineSeries()
        series.color = Color.parseColor("#5C5FBC")

        for (reading in readings) {
                val time = Date(reading.first)
                val calendar = GregorianCalendar.getInstance()
                calendar.time = time
                series.addPoint(
                    ValueLinePoint(
                        (calendar.get(Calendar.DAY_OF_MONTH).toString() + "." + calendar.get(Calendar.MONTH).toString() + ". " + calendar.get(Calendar.HOUR_OF_DAY).toString() + "h"),
                        reading.second
                    )
                )
            }

        lineChart.addSeries(series)
        lineChart.startAnimation()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
    }

}