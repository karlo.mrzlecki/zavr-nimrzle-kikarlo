package com.fer.greenhouse.Models

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log


class DatabaseHelper(context: Context?) :
    SQLiteOpenHelper(
        context,
        TAG, null, 1
    ) {
    override fun onCreate(db: SQLiteDatabase) {
        val createTableReadings =
            "CREATE TABLE " + TABLE_NAME_READINGS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_LOCATIONID + " INT," +
                    COL_LOCATION + " VARCHAR(50)," +
                    COL_SENSOR + " VARCHAR(5)," +
                    COL_READING + " FLOAT," +
                    COL_TIMESTAMP + " TIMESTAMP)"
        val createTableAlerts =
            "CREATE TABLE " + TABLE_NAME_ALERTS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_LOCATIONID + " INT," +
                    COL_LOCATION + " VARCHAR(50) UNIQUE," +
                    COL_AIR_MIN + " FLOAT," +
                    COL_AIR_MAX + " FLOAT," +
                    COL_LIGHT_MIN + " FLOAT," +
                    COL_LIGHT_MAX + " FLOAT," +
                    COL_SOIL_MIN + " FLOAT," +
                    COL_SOIL_MAX + " FLOAT," +
                    COL_TEMP_MIN + " FLOAT," +
                    COL_TEMP_MAX + " FLOAT)"
        db.execSQL(createTableReadings)
        db.execSQL(createTableAlerts)
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME_READINGS")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME_ALERTS")
        onCreate(db)
    }

    fun insertDataReadings(reading: Reading) {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_LOCATIONID, reading.locationId)
        contentValues.put(COL_LOCATION, reading.location)
        contentValues.put(COL_SENSOR, reading.sensor)
        contentValues.put(COL_READING, reading.reading)
        contentValues.put(COL_TIMESTAMP, reading.timestamp.time)
        val result = db.insert(TABLE_NAME_READINGS, null, contentValues)
        if (result == -1.toLong())
            Log.d("ERROR", "inserting to DB readings table failed")
    }

    fun readDataReadings(): MutableList<Reading> {
        val list: MutableList<Reading> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME_READINGS"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val reading = Reading()
                reading.id = result.getInt(0)
                reading.locationId = result.getInt(1)
                reading.location = result.getString(2)
                reading.sensor = result.getString(3)
                reading.reading = result.getFloat(4)
                reading.timestamp.time = result.getLong(5)
                list.add(reading)
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    fun readDataLastReading(): MutableList<Reading> {
        val list: MutableList<Reading> = ArrayList()

        val db = this.readableDatabase
        val query =
            "SELECT DISTINCT $COL_LOCATIONID, $COL_LOCATION, $COL_SENSOR, $COL_READING , MAX($COL_TIMESTAMP) FROM $TABLE_NAME_READINGS GROUP BY $COL_LOCATION, $COL_SENSOR"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val reading = Reading()
                reading.id = 0
                reading.locationId = result.getInt(0)
                reading.location = result.getString(1)
                reading.sensor = result.getString(2)
                reading.reading = result.getFloat(3)
                reading.timestamp.time = result.getLong(4)
                list.add(reading)
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    fun readDataLastReadingByGreenhouse(location: String): MutableList<Reading> {
        val list: MutableList<Reading> = ArrayList()

        val db = this.readableDatabase
        val query =
            "SELECT DISTINCT $COL_LOCATIONID, $COL_LOCATION, $COL_SENSOR, $COL_READING , MAX($COL_TIMESTAMP) FROM $TABLE_NAME_READINGS WHERE $COL_LOCATION= '$location' GROUP BY $COL_LOCATION, $COL_SENSOR"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val reading = Reading()
                reading.id = 0
                reading.locationId = result.getInt(0)
                reading.location = result.getString(1)
                reading.sensor = result.getString(2)
                reading.reading = result.getFloat(3)
                reading.timestamp.time = result.getLong(4)
                list.add(reading)
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    fun readReadingsByGreenhouseBySensor(location: String, sensor: String): MutableList<Pair<Long, Float>> {
        val list: MutableList<Pair<Long, Float>> = ArrayList()

        val db = this.readableDatabase
        val query =
            "SELECT $COL_TIMESTAMP, $COL_READING FROM $TABLE_NAME_READINGS WHERE $COL_LOCATION = '$location' AND $COL_SENSOR = '$sensor' ORDER BY $COL_TIMESTAMP ASC"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                list.add(Pair(result.getLong(0), result.getFloat(1)))
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    fun deleteTableDataReadings() {
        val db = this.readableDatabase
        db.delete(TABLE_NAME_READINGS, null, null)
        db.close()
    }

    fun initAlerts(locationId: Int, location: String): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_LOCATIONID, locationId)
        contentValues.put(COL_LOCATION, location)
        contentValues.put(COL_AIR_MIN, 50)
        contentValues.put(COL_AIR_MAX, 70)
        contentValues.put(COL_LIGHT_MIN, 7000)
        contentValues.put(COL_LIGHT_MAX, 7500)
        contentValues.put(COL_SOIL_MIN, 20)
        contentValues.put(COL_SOIL_MAX, 30)
        contentValues.put(COL_TEMP_MIN, 21)
        contentValues.put(COL_TEMP_MAX, 26)
        val result = db.insert(TABLE_NAME_ALERTS, null, contentValues)
        return if (result == -1.toLong()) {
            Log.d("ERROR", "inserting to DB results table failed")
            false
        } else {
            true
        }
    }

    fun readAlerts(): MutableList<Alerts> {
        val list: MutableList<Alerts> = ArrayList()

        val db = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME_ALERTS"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val alerts = Alerts()
                alerts.id = result.getInt(0)
                alerts.locationId = result.getInt(1)
                alerts.location = result.getString(2)
                alerts.airmin = result.getFloat(3)
                alerts.airmax = result.getFloat(4)
                alerts.lightmin = result.getFloat(5)
                alerts.lightmax = result.getFloat(6)
                alerts.soilmin = result.getFloat(7)
                alerts.soilmax = result.getFloat(8)
                alerts.tempmin = result.getFloat(9)
                alerts.tempmax = result.getFloat(10)
                list.add(alerts)
            } while (result.moveToNext())
        }

        result.close()
        db.close()

        return list
    }

    fun addAlertsOrIgnore(location: String) {
        val db = this.readableDatabase
        val query = "INSERT OR IGNORE INTO " + TABLE_NAME_ALERTS +
                "(" + COL_LOCATION + ", " + COL_AIR_MIN + ", " + COL_AIR_MAX + ", " +
                COL_LIGHT_MIN + ", " + COL_LIGHT_MAX + ", " + COL_SOIL_MIN + ", " +
                COL_SOIL_MAX + ", " + COL_TEMP_MIN + "," + COL_TEMP_MAX + ")" +
                "VALUES ( '" + location + "', " + 50 + ", " + 70 + ", " + 7000 + ", " + 7500 + ", " +
                    20 + ", " + 30 + ", " + 21 + ", " + 26 + ")"
        val result = db.rawQuery(query, null)

        result.close()
        db.close()

    }

    fun updateAlerts(location: String, airmin: Float, airmax: Float, lightmin: Float, lightmax: Float, soilmin: Float, soilmax: Float, tempmin: Float, tempmax: Float){
        val db = this.readableDatabase
        val contentValues = ContentValues()

        contentValues.put(COL_AIR_MIN, airmin)
        contentValues.put(COL_AIR_MAX, airmax)
        contentValues.put(COL_LIGHT_MIN, lightmin)
        contentValues.put(COL_LIGHT_MAX, lightmax)
        contentValues.put(COL_SOIL_MIN, soilmin)
        contentValues.put(COL_SOIL_MAX, soilmax)
        contentValues.put(COL_TEMP_MIN, tempmin)
        contentValues.put(COL_TEMP_MAX, tempmax)

        db.update(TABLE_NAME_ALERTS, contentValues, "location = '" + location + "'", null)
    }


//    fun addData(location: String, sensor: String, reading: Float): Boolean {
//        val db = this.writableDatabase
//        val contentValues = ContentValues()
//        contentValues.put(COL_LOCATION, location)
//        contentValues.put(COL_SENSOR, sensor)
//        contentValues.put(COL_READING, reading)
//        Log.d(
//            TAG,
//            "addData: Adding $location, $sensor, $reading to $TABLE_NAME"
//        )
//        db.insert(TABLE_NAME, null, contentValues)
//        return true
//    }

    /**
     * Returns all the data from database
     * @return
     */
    val data: Cursor
        get() {
            val db = this.writableDatabase
            val query = "SELECT * FROM $TABLE_NAME_READINGS"
            return db.rawQuery(query, null)
        }

    companion object {
        private const val TAG = "DatabaseHelper"
        private const val TABLE_NAME_READINGS = "readings"
        private const val TABLE_NAME_ALERTS = "alerts"
        private const val COL_LOCATIONID = "locationId"
        private const val COL_LOCATION = "location"
        private const val COL_SENSOR = "sensor"
        private const val COL_READING = "reading"
        private const val COL_TIMESTAMP = "timestamp"
        private const val COL_AIR_MIN = "airmin"
        private const val COL_AIR_MAX = "airmax"
        private const val COL_LIGHT_MIN = "lightmin"
        private const val COL_LIGHT_MAX = "lightmax"
        private const val COL_SOIL_MIN = "soilmin"
        private const val COL_SOIL_MAX = "soilmax"
        private const val COL_TEMP_MIN = "tempmin"
        private const val COL_TEMP_MAX = "tempmax"
    }
}