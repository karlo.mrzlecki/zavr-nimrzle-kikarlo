package com.fer.greenhouse.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fer.greenhouse.Models.User
import com.fer.greenhouse.R
import com.fer.greenhouse.REST.ApiClient
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initListeners()
    }

    fun initListeners() {
        btn_register.setOnClickListener {
            val name = et_name.text.toString().trim()
            val username = et_username.text.toString().trim()
            val password = et_password.text.toString().trim()

            if (name.isEmpty()) {
                et_name.error = "Unesite ime i prezime"
                et_name.requestFocus()
            } else if (username.isEmpty()) {
                et_username.error = "Unesite korisničko ime"
                et_username.requestFocus()
            } else if (username.length < 3) {
                et_username.error = "Korisničko ime mora imati barem 3 znaka"
                et_username.requestFocus()
            } else if (!username.matches(("[A-Za-z0-9]+").toRegex())) {
                et_username.error = "Korisničko ime ne smije sadržavati specijane znakove"
                et_username.requestFocus()
            } else if (password.isEmpty()) {
                et_password.error = "Unesite lozinku"
                et_password.requestFocus()
            } else if (password.length < 6) {
                et_password.error = "Lozinka mora imati barem 6 znakova"
                et_password.requestFocus()
            } else {
                val user = User(
                    name,
                    username,
                    password.sha512()
                )
                registerUser(user)
            }

        }

        login.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    fun String.sha512(): String {
        return this.hashWithAlgorithm("SHA-512")
    }

    private fun String.hashWithAlgorithm(algorithm: String): String {
        val digest = MessageDigest.getInstance(algorithm)
        val bytes = digest.digest(this.toByteArray(Charsets.UTF_8))
        return bytes.fold("", { str, it -> str + "%02x".format(it) })
    }

    fun registerUser(user: User) {
        ApiClient.getClient.registerUser(user).enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(
                    this@RegisterActivity,
                    "Neuspješna registracija!",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.code().equals(200)) {
                    Toast.makeText(
                        this@RegisterActivity,
                        "Uspješna registracija!",
                        Toast.LENGTH_SHORT
                    ).show()
                    startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    finish()
                } else {
                    et_username.error = "Korisničko već postoji"
                    et_username.requestFocus()
                    return
                }

            }
        })

    }

}