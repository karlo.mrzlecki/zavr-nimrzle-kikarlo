package com.fer.greenhouse.Models

import com.google.gson.annotations.SerializedName

class GreenhouseId (
    @SerializedName("greenhouseid")
    val greenhouseId: Int?,
    @SerializedName("location")
    val location: String?
)