package com.fer.greenhouse.Models
import com.google.gson.annotations.SerializedName
import java.sql.Timestamp

class ReadingDate (
    @SerializedName("reading")
    val reading: Float?,
    @SerializedName("date")
    val date: Timestamp?
)