package com.fer.greenhouse.REST

import com.fer.greenhouse.Models.GreenhouseId
import com.fer.greenhouse.Models.ReadingDate
import com.fer.greenhouse.Models.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiService {
    @GET("users/{username}")
    fun getUser(@Path("username") username:String): Call<User>
    @POST("users")
    fun registerUser(@Body user : User): Call<User>
    @GET("greenhouse/owner/{username}")
    fun getGreenhouses(@Path("username") username:String): Call<List<GreenhouseId>>
    @GET("readingslimit/{username}/{greenhouseid}/{type}/{noOfReadings}")
    fun getReadings(@Path("username") username:String, @Path("greenhouseid") greenhouseId:Int, @Path("type") type:String, @Path("noOfReadings") noOfReadings: Int): Call<List<ReadingDate>>
}