package com.fer.greenhouse.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.fer.greenhouse.Models.Alerts
import com.fer.greenhouse.Models.DatabaseHelper
import com.fer.greenhouse.Models.Reading
import com.fer.greenhouse.R
import kotlinx.android.synthetic.main.activity_greenhouse.*


class GreenhouseActivity : AppCompatActivity() {

    private var db = DatabaseHelper(this)
    var greenhouse: String? = null
    private var alerts = mutableListOf<Alerts>()
    private var data = mutableListOf<Reading>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_greenhouse)

        alerts = db.readAlerts()
        greenhouse = intent.getStringExtra("greenhouse_title")

        readData()
        initListeners()
        getAlerts()
        checkAlerts(greenhouse!!)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
    }


    private fun readData() {
        data = db.readDataLastReadingByGreenhouse(greenhouse!!)
        tv_greenhouse_title.text = greenhouse
        for (reading in data) {
            if (reading.sensor == "temp") {
                tv_temp.text = (reading.reading.toString() + "°C")
            }
            if (reading.sensor == "soil") {
                tv_soil.text = (reading.reading.toString() + "%")
            }
            if (reading.sensor == "air") {
                tv_air.text = (reading.reading.toString() + "%")
            }
            if (reading.sensor == "light") {
                tv_light.text = (reading.reading.toString() + "lm")
            }
        }
    }

    private fun getAlerts() {
        for (alert in alerts) {
            if (alert.location == greenhouse) {
                tv_air_min.text = (alert.airmin.toInt().toString())
                tv_air_max.text = (alert.airmax.toInt().toString())
                et_air_min.setText(alert.airmin.toInt().toString())
                et_air_max.setText(alert.airmax.toInt().toString())

                tv_light_min.text = (alert.lightmin.toInt().toString())
                tv_light_max.text = (alert.lightmax.toInt().toString())
                et_light_min.setText(alert.lightmin.toInt().toString())
                et_light_max.setText(alert.lightmax.toInt().toString())

                tv_temp_min.text = (alert.tempmin.toInt().toString())
                tv_temp_max.text = (alert.tempmax.toInt().toString())
                et_temp_min.setText(alert.tempmin.toInt().toString())
                et_temp_max.setText(alert.tempmax.toInt().toString())

                tv_soil_min.text = (alert.soilmin.toInt().toString())
                tv_soil_max.text = (alert.soilmax.toInt().toString())
                et_soil_min.setText(alert.soilmin.toInt().toString())
                et_soil_max.setText(alert.soilmax.toInt().toString())
            }
        }
    }

    private fun setAlerts(location: String) {
        db.updateAlerts(
            location,
            et_air_min.text.toString().toFloat(),
            et_air_max.text.toString().toFloat(),
            et_light_min.text.toString().toFloat(),
            et_light_max.text.toString().toFloat(),
            et_soil_min.text.toString().toFloat(),
            et_soil_max.text.toString().toFloat(),
            et_temp_min.text.toString().toFloat(),
            et_temp_max.text.toString().toFloat()
        )
    }

    private fun checkAlerts(location: String) {
        baseContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        for (alert in alerts) {
            for (reading in data) {
                if (reading.locationId == alert.locationId && reading.location == location) {
                    if (reading.sensor == "temp") {
                        if (reading.reading < alert.tempmin || reading.reading > alert.tempmax) {
                            iv_temp_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentRed
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        } else {
                            iv_temp_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentGreen
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        }
                    }
                    if (reading.sensor == "air") {
                        if (reading.reading < alert.airmin || reading.reading > alert.airmax) {
                            iv_air_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentRed
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        } else {
                            iv_air_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentGreen
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        }
                    }
                    if (reading.sensor == "soil") {
                        if (reading.reading < alert.soilmin || reading.reading > alert.soilmax) {
                            iv_soil_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentRed
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        } else {
                            iv_soil_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentGreen
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        }
                    }
                    if (reading.sensor == "light") {
                        if (reading.reading < alert.lightmin || reading.reading > alert.lightmax) {
                            iv_light_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentRed
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        } else {
                            iv_light_status.setColorFilter(
                                ContextCompat.getColor(
                                    applicationContext,
                                    R.color.colorAccentGreen
                                ), android.graphics.PorterDuff.Mode.SRC_IN
                            )
                        }
                    }
                }
            }
        }
    }

    private fun initListeners() {
        btn_edit_alerts.setOnClickListener {
            tv_air_min.visibility = View.GONE
            et_air_min.visibility = View.VISIBLE
            tv_air_max.visibility = View.GONE
            et_air_max.visibility = View.VISIBLE

            tv_temp_min.visibility = View.GONE
            et_temp_min.visibility = View.VISIBLE
            tv_temp_max.visibility = View.GONE
            et_temp_max.visibility = View.VISIBLE

            tv_soil_min.visibility = View.GONE
            et_soil_min.visibility = View.VISIBLE
            tv_soil_max.visibility = View.GONE
            et_soil_max.visibility = View.VISIBLE

            tv_light_min.visibility = View.GONE
            et_light_min.visibility = View.VISIBLE
            tv_light_max.visibility = View.GONE
            et_light_max.visibility = View.VISIBLE

            btn_edit_alerts.visibility = View.GONE
            btn_save_alerts.visibility = View.VISIBLE
        }

        btn_save_alerts.setOnClickListener {
            setAlerts(greenhouse!!)
            alerts = db.readAlerts()
            getAlerts()
            checkAlerts(greenhouse!!)

            tv_air_min.visibility = View.VISIBLE
            et_air_min.visibility = View.GONE
            tv_air_max.visibility = View.VISIBLE
            et_air_max.visibility = View.GONE

            tv_temp_min.visibility = View.VISIBLE
            et_temp_min.visibility = View.GONE
            tv_temp_max.visibility = View.VISIBLE
            et_temp_max.visibility = View.GONE

            tv_soil_min.visibility = View.VISIBLE
            et_soil_min.visibility = View.GONE
            tv_soil_max.visibility = View.VISIBLE
            et_soil_max.visibility = View.GONE

            tv_light_min.visibility = View.VISIBLE
            et_light_min.visibility = View.GONE
            tv_light_max.visibility = View.VISIBLE
            et_light_max.visibility = View.GONE

            btn_edit_alerts.visibility = View.VISIBLE
            btn_save_alerts.visibility = View.GONE
        }

        pullToRefreshGreenhouse.setOnRefreshListener {
            readData()

            getAlerts()
            checkAlerts(greenhouse!!)

            pullToRefreshGreenhouse.isRefreshing = false
        }

        rl_temp.setOnClickListener {
            val intent = Intent(this, GreenhouseChartActivity::class.java)
            intent.putExtra("greenhouse_title", greenhouse)
            intent.putExtra("chart_type", "temp")
            intent.putExtra("chart_name", "temperatura")
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        rl_air.setOnClickListener {
            val intent = Intent(this, GreenhouseChartActivity::class.java)
            intent.putExtra("greenhouse_title", greenhouse)
            intent.putExtra("chart_type", "air")
            intent.putExtra("chart_name", "vlažnost zraka")
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        rl_light.setOnClickListener {
            val intent = Intent(this, GreenhouseChartActivity::class.java)
            intent.putExtra("greenhouse_title", greenhouse)
            intent.putExtra("chart_type", "light")
            intent.putExtra("chart_name", "svjetlosni tok")
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

        rl_soil.setOnClickListener {
            val intent = Intent(this, GreenhouseChartActivity::class.java)
            intent.putExtra("greenhouse_title", greenhouse)
            intent.putExtra("chart_type", "soil")
            intent.putExtra("chart_name", "vlažnost tla")
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }

    }
}
