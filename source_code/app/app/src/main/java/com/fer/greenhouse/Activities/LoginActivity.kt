package com.fer.greenhouse.Activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fer.greenhouse.Models.User
import com.fer.greenhouse.R
import com.fer.greenhouse.REST.ApiClient
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest

class LoginActivity: AppCompatActivity() {
    private var client = ApiClient.getClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListeners()
    }

    private fun initListeners() {
        btn_login.setOnClickListener {
            val username = et_username.text.toString().trim()
            val password = et_password.text.toString().trim()

            if (username.isEmpty()) {
                et_username.error = "Unesite korisničko ime"
                et_username.requestFocus()
            } else if (password.isEmpty()) {
                et_password.error = "Unesite lozinku"
                et_password.requestFocus()
            } else {
                loginUser(username, password)
            }
        }

        register.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    fun String.sha512(): String {
        return this.hashWithAlgorithm("SHA-512")
    }

    private fun String.hashWithAlgorithm(algorithm: String): String {
        val digest = MessageDigest.getInstance(algorithm)
        val bytes = digest.digest(this.toByteArray(Charsets.UTF_8))
        return bytes.fold("", { str, it -> str + "%02x".format(it) })
    }

    fun loginUser(username: String, password: String) {
        val call = client.getUser(username)
        call.enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "Greška!\n" + t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<User>?, response: Response<User>?) {
                if (response == null) {
                    Toast.makeText(this@LoginActivity, "Neuspješna prijava!", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    if (response.isSuccessful) {
                        var user = response.body()
                        if (user?.password != password.sha512()) {
                            Log.d("lozinka", password.sha512())
                            et_password.requestFocus()
                            et_password.error = "Pogrešna lozinka"
                            return
                        } else {
                            val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE).edit()
                            storage.putString("username", user.username)
                            storage.putBoolean("isLoggedIn", true)
                            storage.apply()
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            overridePendingTransition(
                                android.R.anim.fade_in,
                                android.R.anim.fade_out
                            )
                            finish()
                        }
                    } else {
                        et_username.error = "Ne postoji traženo korisničko ime"
                        et_username.requestFocus()
                        return
                    }
                }


            }


        })
    }


}