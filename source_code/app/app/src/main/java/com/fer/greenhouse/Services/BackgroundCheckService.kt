package com.fer.greenhouse.Services

import android.R
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.fer.greenhouse.Activities.MainActivity
import com.fer.greenhouse.Models.*
import com.fer.greenhouse.REST.ApiClient
import retrofit2.Call
import java.sql.Timestamp


class BackgroundCheckService : Service() {
    // Notification settings
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.fer.greenhouse"
    private val description = ""
    private var notificationCount = 0
    private var notificationCountError = 0
    private var firstCall = true


    // Database settings
    private var client = ApiClient.getClient
    var context: Context? = null
    var db = DatabaseHelper(context)
    private var data = mutableListOf<Reading>()
    private var alerts = mutableListOf<Alerts>()
    private var lastReading = mutableListOf<Reading>()
    private var username: String? = ""
    private var delay: Long = 0
    private var connectionError: Long = 0
    private var noOfReadings: Int = 0

    val handler = Handler()

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
        username = storage.getString("username", "")
        delay = storage.getLong("delay", 1800000)
        connectionError = storage.getLong("error", 1800000)
        noOfReadings = storage.getInt("noOfReadings", 24)
    }

    override fun onStartCommand(
        intent: Intent,
        flags: Int,
        startId: Int
    ): Int {
        db = DatabaseHelper(this)
        startForeground()
        handler.postDelayed(object : Runnable {
            override fun run() {

                readData(username!!)
                handler.postDelayed({
                    if (!firstCall) {
                        val lastReading = db.readDataLastReading()
                        for (reading in lastReading) {
                            Log.d("reading", reading.location.toString())
                        }
                        checkAlerts()
                        Log.d("string", "a")
                    }
                    firstCall = false
                }, 1000)
                notificationCount = 0
                handler.postDelayed(this, delay)//30 min delay

            }
        }, 0)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null)
    }


//    private fun timer(){
//        notifyMe()
//        val handler = Handler()
//        handler.postDelayed(object : Runnable {
//            override fun run() {
//                notifyMe()
//                print("a")
//                handler.postDelayed(this, 10000)//1 sec delay
//            }
//        }, 0)
//    }

    private fun notification() {
        notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent = Intent(this, MainActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(
                channelId, description, NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.name = "alertsChannel"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, channelId)
                .setContentTitle("Mjerenja nisu unutar granica!")
                .setContentText("Pogledajte koja mjerenja ne zadovoljavaju postavljene granice.")
                .setColor(Color.parseColor("#5C5FBC"))
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setAutoCancel(true)
        } else {

            builder = Notification.Builder(this)
                .setContentTitle("Mjerenja nisu unutar granica!")
                .setContentText("Pogledajte koja mjerenja ne zadovoljavaju postavljene granice.")
                .setColor(Color.parseColor("#5C5FBC"))
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setAutoCancel(true)
        }
        notificationManager.notify(1234, builder.build())

    }

    private fun notificationReadingError() {
        notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent = Intent(this, MainActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this,
            0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(
                channelId, description, NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.name = "alertsChannel"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, channelId)
                .setContentTitle("Nije bilo mjerenja u zadnjih " + (connectionError/(1000*60)).toString() + " minuta!")
                .setContentText("Provjerite imate li internetsku vezu.")
                .setColor(Color.parseColor("#5C5FBC"))
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setAutoCancel(true)
        } else {

            builder = Notification.Builder(this)
                .setContentTitle("Nije bilo mjerenja u zadnjih " + (connectionError/(1000*60)).toString() + " minuta!")
                .setContentText("Provjerite imate li internetsku vezu.")
                .setColor(Color.parseColor("#5C5FBC"))
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setContentIntent(pendingIntent)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setAutoCancel(true)
        }
        notificationManager.notify(4321, builder.build())

    }

    private fun checkAlerts() {
        var red = 0
        var error = 0
        for (alert in alerts) {
            for (reading in lastReading) {
                if (reading.locationId == alert.locationId) {
                    if (reading.sensor == "temp") {
                        if (reading.reading < alert.tempmin || reading.reading > alert.tempmax) {
                            red++
                        }
                    }
                    if (reading.sensor == "air") {
                        if (reading.reading < alert.airmin || reading.reading > alert.airmax) {
                            red++
                        }
                    }
                    if (reading.sensor == "soil") {
                        red++
                    }
                }
                if (reading.sensor == "light") {
                    red++
                }
                if (Timestamp(System.currentTimeMillis() - connectionError) > reading.timestamp) {
                    error++
                }
            }
            if (error != 0 && notificationCountError == 0){
                notificationReadingError()
            }
            if (red != 0 && notificationCount == 0) {
                notification()
            }
            red = 0
        }
    }


//    private fun notifyMe() {
//        Log.d("notify", "enter")
//        val channelId =
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                createNotificationChannel("notification2", "Notify")
//            } else {
//                ""
//            }
//        val intent = Intent(this, MainActivity::class.java).apply {
//            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        }
//        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
//
//        val builder = NotificationCompat.Builder(this, channelId)
//            .setSmallIcon(R.mipmap.sym_def_app_icon)
//            .setContentTitle("My notification")
//            .setContentText("Hello World!")
//            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//            .setContentIntent(pendingIntent)
//            .setAutoCancel(true)
//
////        notificationManager
//
//        with(NotificationManagerCompat.from(this)) {
//            // notificationId is a unique int for each notification that you must define
//            notify(1, builder.build())
//        }
//    }

    private fun startForeground() {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("bckServ", "backgroundService")
            } else {
                ""
            }
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this, 0,
            notificationIntent, 0
        )
        startForeground(
            NOTIF_ID, NotificationCompat.Builder(
                this,
                channelId
            )
                .setOngoing(true)
                .setSmallIcon(R.mipmap.sym_def_app_icon)
                .setContentTitle("Servis radi u pozadini")
                .setContentText("Pritisnite da bi ste se vratili u aplikaciju")
                .setContentIntent(pendingIntent)
                .build()
        )
    }

    private fun readData(username: String) {
        db.deleteTableDataReadings()

        val call: Call<List<GreenhouseId>> = client.getGreenhouses(username)
        val greenhouses: List<GreenhouseId> = call.execute().body()!!
        for (greenhouse in greenhouses) {
            db.initAlerts(greenhouse.greenhouseId!!, greenhouse.location!!)
            val callReadingsTemp: Call<List<ReadingDate>> =
                client.getReadings(username, greenhouse.greenhouseId, "temp", noOfReadings)
            val readingsTemp: List<ReadingDate> = callReadingsTemp.execute().body()!!
            for (reading in readingsTemp) {
                val readingDb =
                    Reading(
                        greenhouse.greenhouseId,
                        greenhouse.location,
                        "temp",
                        reading.reading!!,
                        reading.date!!
                    )
                db.insertDataReadings(readingDb)
            }
            val callReadingsAir: Call<List<ReadingDate>> =
                client.getReadings(username, greenhouse.greenhouseId, "air", noOfReadings)
            val readingsAir: List<ReadingDate> = callReadingsAir.execute().body()!!
            for (reading in readingsAir) {
                val readingDb =
                    Reading(
                        greenhouse.greenhouseId,
                        greenhouse.location,
                        "air",
                        reading.reading!!,
                        reading.date!!
                    )
                db.insertDataReadings(readingDb)
            }
            val callReadingsSoil: Call<List<ReadingDate>> =
                client.getReadings(username, greenhouse.greenhouseId, "soil", noOfReadings)
            val readingsSoil: List<ReadingDate> = callReadingsSoil.execute().body()!!
            for (reading in readingsSoil) {
                val readingDb =
                    Reading(
                        greenhouse.greenhouseId,
                        greenhouse.location,
                        "soil",
                        reading.reading!!,
                        reading.date!!
                    )
                db.insertDataReadings(readingDb)
            }
            val callReadingsLight: Call<List<ReadingDate>> =
                client.getReadings(username, greenhouse.greenhouseId, "light", noOfReadings)
            val readingsLight: List<ReadingDate> = callReadingsLight.execute().body()!!
            for (reading in readingsLight) {
                val readingDb =
                    Reading(
                        greenhouse.greenhouseId,
                        greenhouse.location,
                        "light",
                        reading.reading!!,
                        reading.date!!
                    )
                db.insertDataReadings(readingDb)
            }
        }

        data = db.readDataReadings()
        alerts = db.readAlerts()
        lastReading = db.readDataLastReading()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    companion object {
        private const val NOTIF_ID = 1
    }
}
