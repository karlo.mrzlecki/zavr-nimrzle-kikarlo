package com.fer.greenhouse.Activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fer.greenhouse.R
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        initListeners()
    }

    private fun initListeners(){
        val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
        val delayBefore = storage.getLong("delay", 1800000)
        val errorBefore = storage.getLong("error", 1800000)
        val readingsBefore = storage.getInt("noOfReadings", 24)

        et_delay.setText((delayBefore/(1000*60)).toString())
        et_error.setText((errorBefore/(1000*60)).toString())
        et_readings.setText(readingsBefore.toString())

        btn_back.setOnClickListener {
            finish()
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            val delay = et_delay.text.toString().toLong()
            val error = et_error.text.toString().toLong()
            val readings = et_readings.text.toString().toInt()
            val editor: SharedPreferences.Editor = storage.edit()
            editor.putLong("delay", delay*60*1000)
            editor.putLong("error", error*60*1000)
            editor.putInt("noOfReadings", readings)
            editor.apply()
        }

        btn_logout.setOnClickListener {
            finish()
            val editor: SharedPreferences.Editor = storage.edit()
            editor.putBoolean("isLoggedIn", false)
            editor.apply()
            val intent = Intent(this, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val storage = getSharedPreferences("STORAGE", Context.MODE_PRIVATE)
        overridePendingTransition(
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        val delay = et_delay.text.toString().toLong()
        val error = et_error.text.toString().toLong()
        val noOfReadings = et_readings.text.toString().toInt()
        val editor: SharedPreferences.Editor = storage.edit()
        editor.putLong("delay", delay*60*1000)
        editor.putLong("error", error*60*1000)
        editor.putInt("noOfReadings", noOfReadings)
        editor.apply()
    }
}
