package com.fer.greenhouse.Models

import java.sql.Date
import java.sql.Timestamp

class Reading{
    var id: Int = 0
    var location: String = ""
    var locationId: Int = 0
    var sensor: String = ""
    var reading: Float = 0.toFloat()
    var timestamp: Timestamp = Timestamp(0)

    constructor(locationId: Int, location: String, sensor: String, reading: Float, timestamp: Timestamp){
        this.locationId = locationId
        this.location = location
        this.sensor = sensor
        this.reading = reading
        this.timestamp = timestamp
    }

    constructor(){
    }
}