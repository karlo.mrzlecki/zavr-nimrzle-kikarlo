package com.greenhouse.backend.controllers

import com.greenhouse.backend.models.Reading
import com.greenhouse.backend.models.ReadingDate
import com.greenhouse.backend.repository.ReadingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.Exception
import java.util.*
import javax.persistence.Tuple

@RestController
class RestControllerReading {
    @Autowired
    lateinit var repository : ReadingRepository

    @GetMapping("/reading")
    fun findall() = repository.findAll()
    @GetMapping("/reading/{id}")
    fun findbyId(@PathVariable id:Long) = repository.findById(id)
    @GetMapping("/readingslimit/{username}/{greenhouse}/{type}/{noOfReadings}")
    fun findByUsernameLimit24(@PathVariable(value = "username") username : String,
                              @PathVariable(value = "greenhouse") greenhouse : Int,
                              @PathVariable(value = "type") type : String,
                              @PathVariable(value = "noOfReadings") noOfReadings : Int) : ResponseEntity<List<ReadingDate>> {
        var user = repository.findReadingLimit(username, greenhouse, type, noOfReadings)
        var response:ResponseEntity<List<ReadingDate>>
        if(user!=null){
            response=ResponseEntity<List<ReadingDate>>(user,HttpStatus.OK)
        }else{
            response=ResponseEntity<List<ReadingDate>>(user,HttpStatus.NOT_FOUND)
        }
        return response
    }
    @PostMapping("/addreading/{sensor}")
    fun addReading(@RequestBody reading : Reading, @PathVariable(value = "sensor") sensor: Long) : ResponseEntity<Reading>{
        var readingid : Long
        var reading = repository.save(reading)
        var response : ResponseEntity<Reading>
        if(reading!=null){
            response = ResponseEntity<Reading>(reading,HttpStatus.OK)
            readingid = response.body!!.readingid
            try{
            repository.addGreenhouseId(sensor, readingid)}
            catch(e: Exception) {}
        }else{
            response = ResponseEntity<Reading>(reading,HttpStatus.FORBIDDEN)
        }
        return response
    }

}
