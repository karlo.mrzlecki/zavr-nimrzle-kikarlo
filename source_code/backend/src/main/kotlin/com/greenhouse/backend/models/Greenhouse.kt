package com.greenhouse.backend.models

import javax.persistence.*

@Entity
@Table(name = "staklenik")
class Greenhouse(
        var location : String,
        var area: Float){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var greenhouseid : Long = 0
    private constructor() : this("",0.toFloat())
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "fk_owner")
//    var owner: User?= null
    @OneToMany // (mappedBy = "greenhouse")
    var sensors: List<Sensor> = emptyList()
//    @OneToOne(mappedBy = "user")
//    var user: List<User> = emptyList()
}