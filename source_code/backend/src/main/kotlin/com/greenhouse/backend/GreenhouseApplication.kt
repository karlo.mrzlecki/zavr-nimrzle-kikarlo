package com.greenhouse.backend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import java.util.*

@SpringBootApplication
@EnableScheduling
open class GreenhouseApplication

fun main(args: Array<String>) {
	runApplication<GreenhouseApplication>(*args)
}
