package com.greenhouse.backend

import com.greenhouse.backend.models.User
import com.greenhouse.backend.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.util.*

@Component
class DbSeeder(val repository : UserRepository) : CommandLineRunner{
	override fun run(vararg p0: String?){
		val admin = User("Vlasnik Staklenika", "admin", "111111")
//		this.repository.save(admin)
		println(" -- Database has been initialized --")
		}
}