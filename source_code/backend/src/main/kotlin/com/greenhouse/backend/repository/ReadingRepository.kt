package com.greenhouse.backend.repository

import com.greenhouse.backend.models.Reading
import com.greenhouse.backend.models.ReadingDate
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*
import javax.persistence.Tuple

interface ReadingRepository : CrudRepository<Reading,Long>{
    @Query(value = "SELECT reading, date FROM korisnik JOIN korisnik_greenhouse on korisnik.userid = korisnik_greenhouse.user_userid \n" +
            "\tJOIN staklenik ON korisnik_greenhouse.greenhouse_greenhouseid = staklenik.greenhouseid\n" +
            "\tNATURAL JOIN staklenik_sensors\n" +
            "\tJOIN senzor ON staklenik_sensors.sensors_sensorid = senzor.sensorid\n" +
            "\tJOIN senzor_readings ON senzor.sensorid = senzor_readings.sensor_sensorid\n" +
            "\tJOIN ocitanje ON senzor_readings.readings_readingid = ocitanje.readingid\n" +
            "\t\tWHERE korisnik.username = ?1 AND staklenik.greenhouseid = ?2 AND senzor.type = ?3 AND ocitanje.date IS NOT NULL\n" +
            "\t\tORDER BY ocitanje.date DESC\n" +
            "\t\tLIMIT ?4", nativeQuery = true)
    fun findReadingLimit(username: String, greenhouse: Int, type: String, noOfReadings: Int): List<ReadingDate>

    @Modifying
    @Query(value = "INSERT INTO senzor_readings (sensor_sensorid, readings_readingid) VALUES (?1, ?2)", nativeQuery = true)
    fun addGreenhouseId(sensorId: Long, readingId: Long): List<Float>

//    @Query(value = "DO \$\$\n" +
//            "DECLARE tableId integer;\n" +
//            "BEGIN\n" +
//            "  INSERT INTO ocitanje (reading) VALUES (?1) RETURNING id INTO tableId;\n" +
//            "  INSERT INTO senzor_readings (sensor_id, readings_id) VALUES (?2, tableId);\n" +
//            "END \$\$;", nativeQuery = true)
//    fun addReading(reading: Float, greenhouse: Int): List<Float>
}