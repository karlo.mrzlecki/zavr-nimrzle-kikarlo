package com.greenhouse.backend.repository

import com.greenhouse.backend.models.Sensor
import org.springframework.data.repository.CrudRepository

interface SensorRepository : CrudRepository<Sensor,Long>{
//    fun findByGreenhouse(greenhouse : Int) : Sensor?
}