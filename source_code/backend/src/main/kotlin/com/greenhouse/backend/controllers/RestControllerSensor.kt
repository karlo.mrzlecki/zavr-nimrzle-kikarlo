//package com.greenhouse.backend.controllers
//
//import com.greenhouse.backend.models.Sensor
//import com.greenhouse.backend.repository.SensorRepository
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.http.HttpStatus
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.GetMapping
//import org.springframework.web.bind.annotation.PathVariable
//import org.springframework.web.bind.annotation.RestController
//
//@RestController
//class RestControllerSensor {
//    @Autowired
//    lateinit var repository : SensorRepository
//
//    @GetMapping("/sensor")
//    fun findall() = repository.findAll()
//    @GetMapping("/sensor/{id}")
//    fun findbyId(@PathVariable id:Long) = repository.findById(id)
//
//}
