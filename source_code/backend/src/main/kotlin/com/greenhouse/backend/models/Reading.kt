package com.greenhouse.backend.models

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "ocitanje")
class Reading(
        var reading : Float,
        var date: Timestamp){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var readingid : Long = 0
    private constructor() : this(0.toFloat(), Timestamp.valueOf(LocalDateTime.now()))
//    @OneToOne(mappedBy = "sensor")
//    var sensor: List<Sensor> = emptyList()

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "fk_sensor")
//    var sensor: Sensor?= null
}