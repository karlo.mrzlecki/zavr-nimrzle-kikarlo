package com.greenhouse.backend.repository

import com.greenhouse.backend.models.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.util.MultiValueMap
import java.util.*


interface UserRepository : CrudRepository<User,Long>{
	fun findByUsername(username : String) : User?
}