package com.greenhouse.backend.models

import com.greenhouse.backend.models.Greenhouse
import com.greenhouse.backend.models.Reading
import javax.persistence.*

@Entity
@Table(name = "senzor")
class Sensor(
        var type : String){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var sensorid : Long = 0
    private constructor() : this("")
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "fk_greenhouse")
//    var greenhouse: Greenhouse?= null
    @OneToMany // (mappedBy = "reading")
    var readings: List<Reading> = emptyList()
//    @OneToOne(mappedBy = "greenhouse")
//    var greenhouse: List<Greenhouse> = emptyList()
}