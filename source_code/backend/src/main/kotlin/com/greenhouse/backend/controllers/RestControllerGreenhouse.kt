package com.greenhouse.backend.controllers

import com.greenhouse.backend.models.Greenhouse
import com.greenhouse.backend.models.GreenhouseId
import com.greenhouse.backend.repository.GreenhouseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class RestControllerGreenhouse {
    @Autowired
    lateinit var repository : GreenhouseRepository

    @GetMapping("/greenhouse")
    fun findall() = repository.findAll()
    @GetMapping("/greenhouse/{id}")
    fun findbyId(@PathVariable id:Long) = repository.findById(id)
    @GetMapping("/greenhouse/{location}")
    fun findByLocation(@PathVariable(value = "location") username : String) : ResponseEntity<Greenhouse> {
        var greenhouse = repository.findByLocation(username)
        var response:ResponseEntity<Greenhouse>
        if(greenhouse!=null){
            response=ResponseEntity<Greenhouse>(greenhouse,HttpStatus.OK)
        }else{
            response=ResponseEntity<Greenhouse>(greenhouse,HttpStatus.NOT_FOUND)
        }
        return response
    }
    @GetMapping("/greenhouse/owner/{username}")
    fun findByOwner(@PathVariable(value = "username") username : String) : ResponseEntity<List<GreenhouseId>> {
        var greenhouse = repository.findByOwner(username)
        var response:ResponseEntity<List<GreenhouseId>>
        if(greenhouse!=null){
            response=ResponseEntity<List<GreenhouseId>>(greenhouse,HttpStatus.OK)
        }else{
            response=ResponseEntity<List<GreenhouseId>>(greenhouse,HttpStatus.NOT_FOUND)
        }
        return response
    }

}
