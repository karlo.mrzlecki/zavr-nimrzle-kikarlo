package com.greenhouse.backend.models
import java.sql.Timestamp

interface ReadingDate {
    val reading: Float?
    val date: Timestamp?
}