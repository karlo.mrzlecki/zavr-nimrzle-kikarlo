package com.greenhouse.backend.repository

import com.greenhouse.backend.models.Greenhouse
import com.greenhouse.backend.models.GreenhouseId
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface GreenhouseRepository : CrudRepository<Greenhouse,Long>{
    fun findByLocation(location : String) : Greenhouse?

    @Query(value = "SELECT DISTINCT(staklenik.greenhouseid), staklenik.location FROM korisnik NATURAL JOIN korisnik_greenhouse NATURAL JOIN staklenik\n" +
            "\t\tWHERE korisnik.username = ?1", nativeQuery = true)
    fun findByOwner(username: String): List<GreenhouseId>
}