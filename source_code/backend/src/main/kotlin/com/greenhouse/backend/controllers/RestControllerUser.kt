package com.greenhouse.backend.controllers

import com.greenhouse.backend.models.User
import com.greenhouse.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class RestControllerUser {
	@Autowired
	lateinit var repository : UserRepository

	@GetMapping("/users")
	fun findall() = repository.findAll()
	@PostMapping("/users")
	fun addUser(@RequestBody user : User) : ResponseEntity<User>{
		val response : ResponseEntity<User>
		if(repository.findByUsername(user.username)==null){
			repository.save(user)
			response = ResponseEntity<User>(user,HttpStatus.OK)
		}else{
			response = ResponseEntity<User>(user,HttpStatus.FORBIDDEN)
		}
		return response
	}
	@GetMapping("/userid/{id}")
	fun findbyId(@PathVariable id:Long) = repository.findById(id)
	@GetMapping("/users/{username}")
	fun findByUsername(@PathVariable(value = "username") username : String) : ResponseEntity<User> {
		var user = repository.findByUsername(username)
		var response:ResponseEntity<User>
		if(user!=null){
			response=ResponseEntity<User>(user,HttpStatus.OK)
		}else{
			response=ResponseEntity<User>(user,HttpStatus.NOT_FOUND)
		}
		return response
	}
}
