package com.greenhouse.backend.models

interface GreenhouseId {
    val greenhouseid: Int?
    val location: String?
}