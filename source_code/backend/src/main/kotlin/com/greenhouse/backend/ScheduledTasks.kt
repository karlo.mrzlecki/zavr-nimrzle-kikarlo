package com.greenhouse.backend

import com.jayway.restassured.RestAssured
import com.jayway.restassured.response.Response
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat


@Component
class ScheduledTasks {
    private val log: Logger = LoggerFactory.getLogger(ScheduledTasks::class.java)

    // Greenhouse "Radoboj"
    // Temperature sensor
    @Scheduled(fixedRate = 3600000)
    fun TemperatureSensor1() {
        val random = (10..40).random()
        log.info("Temperature greenhouse \"Radoboj\"" + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/1")
    }
    //Air humidity sensor
    @Scheduled(fixedRate = 3600000)
    fun AirHumidity1() {
        val random = (0..100).random()
        log.info("Air humidity greenhouse \"Radoboj\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/2")
    }
    //Soil humidity sensor
    @Scheduled(fixedRate = 3600000)
    fun SoilHumidity1() {
        val random = (0..100).random()
        log.info("Soil humidity greenhouse \"Radoboj\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/3")
    }
    //Light sensor
    @Scheduled(fixedRate = 3600000)
    fun Light1() {
        var random = (10..1500).random()
        random *= 10
        log.info("Soil humidity greenhouse \"Radoboj\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/4")
    }

    // Greenhouse "Pregrada"
    // Temperature sensor
    @Scheduled(fixedRate = 3600000)
    fun TemperatureSensor2() {
        val random = (10..40).random()
        log.info("Temperature greenhouse \"Pregrada\"" + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/5")
    }
    //Air humidity sensor
    @Scheduled(fixedRate = 3600000)
    fun AirHumidity2() {
        val random = (0..100).random()
        log.info("Air humidity greenhouse \"Pregrada\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/6")
    }
    //Soil humidity sensor
    @Scheduled(fixedRate = 3600000)
    fun SoilHumidity2() {
        val random = (0..100).random()
        log.info("Soil humidity greenhouse \"Pregrada\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/7")
    }
    //Light sensor
    @Scheduled(fixedRate = 3600000)
    fun Light2() {
        var random = (10..1500).random()
        random *= 10
        log.info("Soil humidity greenhouse \"Pregrada\": " + random.toString())

        RestAssured.baseURI = "http://127.0.0.1:8080/addreading"
        val request = RestAssured.given()

        request.header("Content-Type", "application/json");
        request.body("{\"reading\": " + random + "}")
        request.post("/8")
    }
}