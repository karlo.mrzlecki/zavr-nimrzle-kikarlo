package com.greenhouse.backend.models

import com.greenhouse.backend.models.Greenhouse
import javax.persistence.*

@Entity
@Table (name = "korisnik")
class User(
	var name : String,
	@Column(unique=true)
	var username: String,
	var password: String){
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var userid : Long = 0
	private constructor() : this("","","")
	@OneToMany // (mappedBy = "owner")
	var greenhouse: List<Greenhouse> = emptyList()
}