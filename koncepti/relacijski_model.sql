CREATE TABLE "STAKLENIK" (
  "id" SERIAL PRIMARY KEY,
  "lokacija" varchar,
  "povrsina" int
);

CREATE TABLE "KORISNIK" (
  "id" int PRIMARY KEY,
  "ime" varchar,
  "prezime" varchar
);

CREATE TABLE "SENZOR" (
  "id" int PRIMARY KEY,
  "vrsta_senzora" varchar
);

CREATE TABLE "OCITANJE" (
  "id" int PRIMARY KEY,
  "rezultat_ocitanja" float,
  "vrijeme_ocitanja" timestamp DEFAULT current_timestamp
);

CREATE TABLE "pregledava" (
  "id_korisnik" int,
  "id_staklenik" int,
  FOREIGN KEY ("id_korisnik") REFERENCES "KORISNIK" ("id"),
  FOREIGN KEY ("id_staklenik") REFERENCES "STAKLENIK" ("id")
);

CREATE TABLE "postoje_senzori" (
  "id_staklenik" int,
  "id_senzor" int,
  FOREIGN KEY ("id_staklenik") REFERENCES "STAKLENIK" ("id"),
  FOREIGN KEY ("id_senzor") REFERENCES "SENZOR" ("id")
);

CREATE TABLE "senzor_ocitanje" (
  "id_senzor" int,
  "id_ocitanje" int,
  FOREIGN KEY ("id_senzor") REFERENCES "SENZOR" ("id"),
  FOREIGN KEY ("id_ocitanje") REFERENCES "OCITANJE" ("id")
);
